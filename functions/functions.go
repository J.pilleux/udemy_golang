package functions

import "fmt"

func printInfoNoParam() {
	fmt.Printf("Name=%s, age=%d, email=%s\n", "Billy", 10, "billy@golang.org")
}

func printInfoParams(name string, age int, email string) {
	fmt.Printf("Name=%s, age=%d, email=%s\n", name, age, email)
}

func avg(x, y float64) float64 {
	return (x + y) / 2
}

func sumNamedReturn(x, y, z int) (sum int) {
	sum = x + y + z // La variable sum est déjà déclarée dans la définition de fonction
	return          // La dernière variable utilisée
}

func FunctionMain() {
	printInfoNoParam()
	printInfoParams("Bobby", 15, "bobby@golang.org")
	result := avg(16.3, 25.0)
	fmt.Printf("Average result=%f\n", result)

	sum := sumNamedReturn(10, 27, 7) // Ici on doit déclarer
	fmt.Printf("Sum result=%d\n", sum)
}
