package functionliteral

import (
	"fmt"
	"sort"
)

func FunctionliteralMain() {
	// Simple definition
	func() {
		fmt.Println("Hello from literal!")
	}()

	// With argument
	func(msg string) {
		fmt.Println(msg)
	}("Hello from literal with parameter!")

	// Used for sorting a list
	people := []string{"Billy", "Bil", "Johnny"}
	sort.Slice(people, func(i, j int) bool {
		return len(people[i]) < len(people[j])
	})

	fmt.Printf("Sorted people: %v\n", people)

	// Lambda can be stored in a variable
	less := func(i, j int) bool {
		return len(people[i]) < len(people[j])
	}
	sort.Slice(people, less)
}
