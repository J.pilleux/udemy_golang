package arrays

import "fmt"

/* Un slice est en quelque sorte un tableau de taille dynamique
s := make([]type, taille, capacity)
s := make([]int, 3)
s = append(s, 12)
len(s) // 4
cap(s) // 6, un tableau avec la double de capacitée est alloué
Un slice pointe directement sur un tableau
*/

func SliceMain() {
	nums := make([]int, 2, 3)
	nums[0] = 10
	nums[1] = -1
	fmt.Printf("%d (len=%d, cap=%d)\n", nums, len(nums), cap(nums))

	nums = append(nums, 64)
	fmt.Printf("%d (len=%d, cap=%d) capacity unchanged\n", nums, len(nums), cap(nums))

	nums = append(nums, 42)
	// Un nouveau tableau est alloué avec toutes les valeurs de l'ancien
	fmt.Printf("%d (len=%d, cap=%d) capacity is doubled\n", nums, len(nums), cap(nums))

	letters := []string{"g", "o", "l", "a", "n", "g"}
	fmt.Printf("%s (len=%d, cap=%d)\n", letters, len(letters), cap(letters))
	fmt.Printf("--------------------\n")

	letters = append(letters, "!")
	fmt.Printf("%s (len=%d, cap=%d)\n", letters, len(letters), cap(letters))

	sub1 := letters[0:2]
	sub2 := letters[2:]
	fmt.Printf("%s (len=%d, cap=%d)\n", sub1, len(sub1), cap(sub1))
	fmt.Printf("%s (len=%d, cap=%d)\n", sub2, len(sub2), cap(sub2))

	sub2[0] = "UP" // les autres tableaux sont modifiés
	fmt.Printf("%s (len=%d, cap=%d)\n", sub1, len(sub1), cap(sub1))
	fmt.Printf("%s (len=%d, cap=%d)\n", sub2, len(sub2), cap(sub2))
	fmt.Printf("%s (len=%d, cap=%d)\n", letters, len(letters), cap(letters))
	fmt.Printf("--------------------\n")

	subCopy := make([]string, len(sub1))
	copy(subCopy, sub1)
	subCopy[0] = "DOWN"
	fmt.Printf("COPY: %s (len=%d, cap=%d)\n", subCopy, len(subCopy), cap(subCopy))
}
