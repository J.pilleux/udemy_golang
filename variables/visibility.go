package variables

// Public becaus in PascalCase
var Name = "Billy"
var Age = 10

// Private because in snakeCase
var password = "secret password"
