package functions

import (
	"fmt"
	"strings"
)

func ToLowerStr(name string) (string, int) {
	return strings.ToLower(name), len(name)
}

func MultipleReturnsMain() {
	lowerName, nameLen := ToLowerStr("Billy")
	fmt.Printf("%s (len=%d)\n", lowerName, nameLen)

	_, nameLen = ToLowerStr("Bobby") // Ignore the first value returned
}
