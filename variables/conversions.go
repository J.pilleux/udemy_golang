package variables

import "fmt"

func TestConversion() {
	var percentage float64 = 2.5

	// percentage = percentage + 34
	percentage += 34 // On peut ajouter un entier à un float, l'inverse n'est pas vrai
	fmt.Printf("Current percentage %f%%\n", percentage)
	fmt.Printf("Int value=%d%%\n", int(percentage)) // Cast in Integer

	n := 43
	f := float64(n) + 0.42 // On doit cast avant d'ajouter un float
	fmt.Printf("f: %f\n", f)
}
