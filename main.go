package main

import (
	"fmt"
<<<<<<< HEAD
	"udemy/helloworld/anonymstruct"
=======
>>>>>>> 0fe8b19702af73fc703b685c17bc2e87cba4fdf5
	"udemy/helloworld/teststruct/player"
	"udemy/helloworld/teststruct/user"
	"udemy/helloworld/variables"
)

func testStruct() {
	var p1 player.Player
	p1.Name = "Billy"
	p1.Age = 10

	fmt.Printf("Player 1: %v\n", p1)
	fmt.Printf("p1.Name=%v, p1.Age=%v\n", p1.Name, p1.Age)

	a := player.Avatar{"http://avatar.jpg"}
	fmt.Printf("a: %v\n", a)

	p2 := player.Player{
		Name: "Bobby",
		Age:  25,
		Avatar: player.Avatar{
			Url: "http://url.com",
		},
	}

	fmt.Printf("Player 2: %v\n", p2)

	p3 := player.New("Johnny")
	fmt.Printf("Player 3: %v\n", p3)

}

func testVariables() {
	fmt.Println(variables.Name, variables.Age)
	// cannot use password
	// fmt.Println(variables.password)
}

func main() {
	//udemylogs.LogsMain()
	//functionliteral.FunctionliteralMain()
	// highorder.HighOrderMain()
	// user.UserMain()
	anonymstruct.AnonymStructMain()
}
