package errmanage

import (
	"fmt"
	"io/ioutil"
)

func readFile(filename string) (string, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}

	if len(data) == 0 {
		return "", fmt.Errorf("File (%s) is empty", filename)
	}

	return string(data), nil
}

func ErrmanageMain() {
	data, err := readFile("test.txt")
	if err != nil {
		fmt.Printf("Error while reading file: %v\n", err)
		return
	}

	fmt.Println("File content:")
	fmt.Println(data)
}
