package loops

import (
	"fmt"
)

func forLoops() {
	sum := 0
	for i := 1; i <= 10; i++ {
		sum += i
	}

	fmt.Printf("sum: %d\n", sum)

	eventCount := 0
	for eventCount < 3 {
		fmt.Println("Retrieving events...")
		eventCount++
		if eventCount == 3 {
			fmt.Printf("Got %d notifications\n", eventCount)
		}
	}

	i := 0
	for {
		i++
		if i%2 != 0 {
			fmt.Println("Odd looping")
			continue
		}
		fmt.Println("Looping...")

		if i >= 10 {
			fmt.Println("Loop end")
			break
		}
	}
}

func rangeTest() {
	names := []string{"Billy", "Bobby", "Johnny", "Robert"}
	for i, n := range names {
		fmt.Printf("Usernames=%s (index=%d)\n", n, i)
	}

	for _, c := range "golang" {
		fmt.Printf("c: %c\n", c)
	}
}

func LoopsMain() {
	forLoops()
	rangeTest()
}
