package interfaces

import "fmt"

type Animal interface {
	Speak() string
}

type Dog struct {
	color string
}

func (d Dog) Speak() string {
	return "Woof!"
}

type Cat struct {
	jumpHeight int
}

func (c Cat) Speak() string {
	return "Miaou!"
}

func TypeMain() {
	animals := []Animal{
		Dog{"White"},
		Cat{2},
	}

	for _, a := range animals {
		fmt.Println(a.Speak())
		fmt.Printf("Type of animal? %T\n", a)

		if t, ok := a.(Dog); ok {
			fmt.Printf("We have a dog! color=%v\n", t.color)
		} else {
			fmt.Println("It's not a dog")
		}
		// fmt.Printf("Type assertion value=%v, ok=%v\n", t, ok)
	}

	fmt.Println("-------------------")

	for _, a := range animals {
		describeAnimal(a)
	}
}

func describeAnimal(a Animal) {
	switch v := a.(type) {
	case Dog:
		fmt.Printf("It is a dog: color=%s\n", v.color)
	case Cat:
		fmt.Printf("It is a cat: jumpHeight=%d\n", v.jumpHeight)
	default:
		fmt.Println("Not a known animal")
	}
}
