package arrays

import "fmt"

func ArraysMain() {
	var names [3]string
	fmt.Printf("names=%v (len=%d)\n", names, len(names))

	names[0] = "Billy"
	names[2] = "Bobby"
	fmt.Printf("names=%v (len=%d)\n", names, len(names))
	fmt.Printf("names[2]=%s\n", names[2])

	odds := [4]int{1, 3, 5, 7}
	fmt.Printf("odds=%v (len=%d)\n", odds, len(odds))
}
