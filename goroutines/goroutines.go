package goroutines

import (
	"fmt"
	"time"
)

// Goroutines
func longOperation(duration int) {
	time.Sleep(time.Duration(duration) * time.Second)
	fmt.Printf("Operation finished took %d s\n", duration)
}

func basic() {
	fmt.Println("Starting first operation")
	go longOperation(1)

	fmt.Println("Starting second operation")
	longOperation(1)

	time.Sleep(2 * time.Second)
}

// Channels
func ping(c chan string) {
	for i := 1; ; i++ {
		c <- fmt.Sprintf("ping %v", i)
	}
}

func pong(c chan string) {
	for i := 100; ; i++ {
		c <- fmt.Sprintf("pong %v", i)
	}
}

func printRes(c chan string) {
	for {
		msg := <-c
		fmt.Println(msg)
		time.Sleep(1 * time.Second)
	}
}

func chanels() {
	c := make(chan string)
	go ping(c)
	go pong(c)
	go printRes(c)

	time.Sleep(10 * time.Second)
}

// Deadlock
func hello(c chan string) {
	c <- "Hello there"
	fmt.Println("hello finished")
}

func deadlock() {
	c := make(chan string)
	go hello(c)
	c <- "Message from main"

	time.Sleep(1 * time.Second)
}

// Unidirectional, this is a write only channel
func operationChannel(done chan<- bool) {
	fmt.Println("Start")
	time.Sleep(2 * time.Second)
	fmt.Println("Back to main")
	done <- true
}

func unidirectionalChannel() {
	done := make(chan bool)
	go operationChannel(done)
	<-done
	fmt.Println("Finished")
}

// Ranges et channels
func reader(c chan string) {
	fmt.Println("Start read")
	defer fmt.Println("Stop read")
	for n := range c {
		fmt.Println(n)
	}
}

func ranges() {
	c := make(chan string)
	go reader(c)

	c <- "Billy"
	c <- "Bobby"
	close(c)       // Usualy, this is the writer that close the channel
	c <- "Unknown" // Panics

	time.Sleep(5 * time.Second)
}

// Buffured channels
func buffuredWriter(c chan string) {
	names := []string{"billy", "bobby", "toto", "tutu"}
	for _, n := range names {
		c <- n
		fmt.Printf("Wrote %v to channel (len=%v)\n", n, len(c))
	}
	close(c)
}

func buffuredChannels() {
	c := make(chan string, 3)
	fmt.Printf("Channel data: cap=%v, len=%v\n", cap(c), len(c))

	go buffuredWriter(c)
	time.Sleep(2 * time.Second)

	for v := range c {
		fmt.Printf("read value %v (len=%v)\n", v, len(c))
		time.Sleep(1 * time.Second)
	}
}

// Select
func client1(c chan string) {
	for i := 0; i < 5; i++ {
		c <- fmt.Sprintf("Message from client1 => %v", i)
		time.Sleep(1500 * time.Millisecond)
	}
}

func client2(c chan string) {
	for i := 10; i < 15; i++ {
		c <- fmt.Sprintf("Message from client2 => %v", i)
		time.Sleep(3000 * time.Millisecond)
	}
}

func selectTest() {
	c1 := make(chan string)
	go client1(c1)
	c2 := make(chan string)
	go client2(c2)

	maxEmpty := 10
	currEmpty := 0

	for currEmpty <= maxEmpty {
		time.Sleep(1 * time.Second)
		select {
		case v := <-c1:
			fmt.Printf("Message received from client1: %v\n", v)
		case v := <-c2:
			fmt.Printf("Message received from client2: %v\n", v)
		default:
			fmt.Println("Nothing to read")
			currEmpty++
		}
	}
}

// Les goroutines ne sont pas des threads
// Les threads ne sont pas gérés par le système, mais un ordonanceur propre à Go (dans le runtime Go)
// Elles sont plus légères en mémoire
// Elles se lancent plus vite
// Il n'y a pas forcément autant de sécurité apportée aux goroutines qu'aux threads vu que c'est pas géré par l'OS
func GoroutinesMain() {
	selectTest()
}
