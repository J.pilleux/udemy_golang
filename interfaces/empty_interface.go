package interfaces

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Cooker interface {
	Cook()
}

type Cheif struct {
	Person
	Stars int
}

func (c Cheif) Cook() {
	fmt.Printf("I'm cooking with %v stars\n", c.Stars)
}

func processPerson(i interface{}) {
	switch p := i.(type) {
	case Person:
		fmt.Printf("We have a person=%v\n", p)
	case Cheif:
		fmt.Printf("We have a chef=%v, let him cook...\n", p)
		p.Cook()
	default:
		fmt.Printf("Unknown type=%T, value=%v\n", p, p)
	}
}

func EmptyInterfaceMain() {
	var x interface{} = Person{"Bob", 10}
	fmt.Printf("x type=%T, data=%v\n", x, x)

	s, ok := x.(string)
	fmt.Printf("Person as string ok? %v. value='%v'\n", ok, s)

	processPerson(x)

	x = Cheif{
		Stars: 4,
		Person: Person{
			Name: "Billy",
			Age:  22,
		},
	}

	processPerson(x)
	processPerson(3)
	processPerson("Bobby")
}
