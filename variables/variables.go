package variables

import "fmt"

// Déclarere une variable en dehors des fonctions
// Il est juste impossible de déclarer avec le « := »
var pi float32 = 3.14

func TutoVariables() {
	// Déclaration de variables
	var age int = 20
	var age_ int // 0
	fmt.Println(age, age_)

	// Deux à la même ligne
	var weight, height int = 80, 190
	fmt.Println(weight, height)

	// Sans type
	var name = "Billy"
	fmt.Println(name)

	// Raccourci
	email := "billy@go.org"
	// Impossible de déclarer deux fois une variable
	// emain := "other@email.org"
	fmt.Println(email)

	fmt.Println(pi)
}
