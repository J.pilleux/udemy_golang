package user

import "fmt"

type User struct {
	Name  string
	Email string
}

func (u *User) SayHello() {
	fmt.Printf("My name is %s\n", u.Name)
}

type Admin struct {
	User
	Level int
}

func (a Admin) SayHello() {
	fmt.Printf("My admin name is %s\n", a.Name)
}

func UserMain() {
	u := User{
		Name:  "Billy",
		Email: "billy@golang.org",
	}
	fmt.Printf("u: %v\n", u)

	a := Admin{
		Level: 2,
		User: User{
			Name:  "Billy",
			Email: "billy@golang.org",
		},
	}

	fmt.Printf("a: %v\n", a)
	a.SayHello()
}
