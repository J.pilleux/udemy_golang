package udemylogs

func LogsMain() {
	initLoggers()

	InfoLogger.Println("Info message")
	WarningLogger.Println("Warning message")
	ErrorLogger.Println("Error message")
}
