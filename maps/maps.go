package maps

import "fmt"

func basic() {
	m := make(map[string]int)

	fmt.Printf("Content=%v (len=%v)\n", m, len(m))

	m["hello"] = 5
	m["goodbye"] = 10

	fmt.Printf("Content=%v (len=%v)\n", m, len(m))
	fmt.Printf("Key=hello, value=%v\n", m["hello"])

	i := m["goodbye"]
	fmt.Println(i)

	j, ok := m["helo"]
	fmt.Printf("j=%v, ok=%v\n", j, ok)

	m["beatles"] = 2
	if _, ok := m["beatles"]; ok {
		fmt.Println("beatles key exists")
	}

	delete(m, "beatles")
	fmt.Printf("Content=%v (len=%v)\n", m, len(m))

	m2 := m
	fmt.Printf("m2 content %v (len=%v)\n", m2, len(m2))
	m2["help"] = 44

	fmt.Printf("Content=%v (len=%v)\n", m, len(m))
	fmt.Printf("m2 content %v (len=%v)\n", m2, len(m2))

}

func parcours() {
	m := map[string]int{
		"Billy": 10,
		"Bobby": 15,
	}

	for name, age := range m {
		fmt.Printf("name=%v, age=%v\n", name, age)
	}

	i := 1
	for name := range m {
		fmt.Printf("name=%v\n", name)
		m[name] = i
	}

	fmt.Printf("m: %v\n", m)
}

type User struct {
	name string
}

func withStructs() {
	m := map[string]*User{
		"HR":  {"Billy"},
		"CEO": {"Bobby"},
	}

	fmt.Println(m["HR"])

	hr := m["HR"]
	hr.name = "Johnny"

	fmt.Println(m["HR"])

	m["CFO"] = &User{"Robert"}
	fmt.Printf("m: %v\n", m)
}

type Key struct {
	ID   int
	Name string
}

func compositeKey() {
	res := make(map[Key]string)
	res[Key{1, "BÉPO"}] = "file"

	k := Key{2, "AUIE"}
	res[k] = "file2"

	delete(res, Key{1, "BÉPO"})
	fmt.Printf("res: %v\n", res)
}

func MainMaps() {
	basic()
	parcours()
	withStructs()
	compositeKey()
}
