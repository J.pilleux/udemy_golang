package pointers

import "fmt"

func updateVal(val string) {
	val = "Johnny"
}

func updatePtr(val *string) {
	*val = "Johnny"
}

func PointerMain() {
	i := 1
	var p *int = &i

	fmt.Printf("p: %v\n", i)
	fmt.Printf("p: %v\n", p)
	fmt.Printf("*p: %v\n", *p)
	fmt.Println("--------------")

	s := "Billy"
	sptr := &s
	s2 := *sptr

	fmt.Println("String pointer")
	fmt.Printf("s=%v\n", s)
	fmt.Printf("s=%v\n", sptr)
	fmt.Printf("*s=%v\n", *sptr)
	fmt.Printf("s2=%v\n", s2)
	fmt.Println("--------------")

	*sptr = "Bobby"

	fmt.Println("Dereference and update")
	fmt.Printf("s=%v\n", s)
	fmt.Printf("s=%v\n", sptr)
	fmt.Printf("*s=%v\n", *sptr)
	fmt.Printf("s2=%v\n", s2)
	fmt.Println("--------------")

	updateVal(s)
	fmt.Println("Update value")
	fmt.Printf("s: %v\n", s)
	fmt.Printf("*sptr: %v\n", *sptr)
	fmt.Println("--------------")

	updatePtr(&s)
	fmt.Println("Update value")
	fmt.Printf("s: %v\n", s)
	fmt.Printf("*sptr: %v\n", *sptr)
	fmt.Println("--------------")
}
