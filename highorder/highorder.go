package highorder

import "fmt"

type student struct {
	name       string
	grade      int
	attendance int
}

func filter(students []student, f func(s student) bool) []student {
	var arr []student
	for _, s := range students {
		if f(s) {
			arr = append(arr, s)
		}
	}
	return arr
}

func HighOrderMain() {
	s1 := student{
		name:       "Billy",
		grade:      4,
		attendance: 8,
	}
	s2 := student{
		name:       "Bobby",
		grade:      8,
		attendance: 4,
	}
	s := []student{s1, s2}
	f := filter(s, func(s student) bool {
		return s.grade >= 5
	})

	fmt.Printf("Student filter simple: %v\n", f)

	// Generating a function in return
	filterFunc := genFilterFunc("Z")
	f = filter(s, filterFunc)
	fmt.Printf("Studens filter school Z: %v\n", f)

	filterFunc = genFilterFunc("A")
	f = filter(s, filterFunc)
	fmt.Printf("Studens filter school A: %v\n", f)

	filterFunc = genFilterFunc("B")
	f = filter(s, filterFunc)
	fmt.Printf("Studens filter school B: %v\n", f)
}

func genFilterFunc(schoolName string) func(s student) bool {
	switch schoolName {
	case "A":
		return func(s student) bool {
			return s.grade >= 5
		}
	case "B":
		return func(s student) bool {
			return s.attendance >= 5
		}
	default:
		return func(s student) bool { return true }
	}
}
