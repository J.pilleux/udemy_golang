package flowcontrol

import "fmt"

func TestIf() {
	day := 10
	// day of a month
	if day < 15 {
		fmt.Printf("We are in the first half of the month (day = %d)\n", day)
	} else if day == 18 {
		fmt.Printf("We are in the special day (day=%d)\n", day)
	} else {
		fmt.Printf("We are in the second half of the month (day=%d)\n", day)
	}
}

func TestComplexIf() {
	year, month, day := 2009, 11, 10
	fmt.Printf("Date=%d/%d/%d\n", day, month, year)

	if year == 2009 && month == 11 && day == 10 {
		fmt.Println("This is the first release of Go!")
	} else if year == 2009 || month == 11 || day == 10 {
		fmt.Println("At least one part is right")
	} else {
		fmt.Println("Just another day")
	}
}

func InlineIf() {
	if count := 12; count > 10 {
		fmt.Printf("We have enough count. got =%d\n", count)
	} else {
		fmt.Printf("Not enough. got=%d\n", count)
	}

	// count cannot be accessed here
	// fmt.Println(count) <- ERROR
}

// So long Python !
func TestSwitch() {
	weekDay := 6 // 1 == Monday, 7 == Sunday
	fmt.Printf("Day of the week=%d, what is so special today\n", weekDay)

	switch weekDay {
	case 1:
		fmt.Println("Beginning of the week, let's get to work")
	case 3:
		fmt.Println("Wednesday, the half is done!")
	case 6, 7: // Plus d'une valeur dans le case
		fmt.Println("It is the week-end!")
	default:
		fmt.Println("Nothing special about this day")
	}

	hour := 10
	fmt.Printf("Current time=%d\n", hour)
	switch {
	case hour < 12:
		fmt.Println("It is the morning")
	case hour > 12 && hour < 18:
		fmt.Println("It is the afternoon")
	default:
		fmt.Println("It is the evening")
	}
}
