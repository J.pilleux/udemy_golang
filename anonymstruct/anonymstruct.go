package anonymstruct

import (
	"html/template"
	"os"
)

var tpl = `
======= {{.Name}} =======
Grade: {{.Grade}}
Attendance: {{.Attendance}}
School: {{.School}}
`

func AnonymStructMain() {
	t, _ := template.New("student").Parse(tpl)
	student := struct {
		Name       string
		Grade      int
		Attendance int
		School     string
	}{
		"Billy",
		8,
		4,
		"A",
	}

	t.Execute(os.Stdout, student)
}
