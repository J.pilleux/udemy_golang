package receivers

import "fmt"

type Rect struct {
	Width, Height int
}

func (r Rect) Area() int {
	return r.Height * r.Width
}

func (r Rect) DoubleSize() {
	r.Width *= 2
	r.Height *= 2
	fmt.Println("DoubleSize()", r)
}

// Va surcharger l'affichage avec %v
func (r Rect) String() string {
	return fmt.Sprintf("Height=%v Width=%v", r.Height, r.Width)
}

func MainReceiver() {
	r := Rect{2, 4}
	fmt.Printf("Rect area=%v\n", r.Area())
	fmt.Printf("r: %v\n", r)

	r.DoubleSize()
	fmt.Printf("r: %v\n", r)
}
