package defertest

import "fmt"

func start() {
	fmt.Println("Start")
}

func finish() {
	fmt.Println("Finish")
}

func DeferTestMain() {
	start()
	defer finish()

	names := []string{"Billy", "Bobby", "Johnny", "Robert"}
	for _, n := range names {
		fmt.Printf("name=%v\n", n)
		defer fmt.Printf("Goodbye %v\n", n) // Il empile les defers (LIFO)
	}
}
