package teststruct

type Address struct {
	street, city string
}

type Person struct {
	Name string
	Age  int
	Addr Address
}

func TestStructMain() {
	var p Person
	p.Name = "Billy"
	p.Addr.city = "Chicago"
}
