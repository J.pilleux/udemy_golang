package receivers

import (
	"fmt"
	"strings"
)

type Post struct {
	Title     string
	Text      string
	published bool
}

func (p Post) Headline() string {
	return fmt.Sprintf("%v - %v", p.Title, p.Text[:10])
}

func (p Post) Published() bool {
	return p.published
}

func (p *Post) Publish() {
	p.published = true
}

func (p *Post) Unpublish() {
	p.published = false
}

func UpperTitle(p *Post) {
	p.Title = strings.ToUpper(p.Title)
}

func MainPointerReceiver() {
	p := Post{
		Title: "Go release",
		Text:  `This is a multiline text`,
	}

	fmt.Printf("p: %v\n", p)

	headline := p.Headline()
	fmt.Printf("headline: %v\n", headline)

	p.Publish()
	fmt.Printf("p: %v\n", p)

	UpperTitle(&p)
	fmt.Printf("p: %v\n", p)

	pythonPost := &Post{
		Title: "Python Intro",
		Text:  "Text",
	}

	UpperTitle(pythonPost)
	fmt.Printf("pythonPost: %v\n", *pythonPost)
}
