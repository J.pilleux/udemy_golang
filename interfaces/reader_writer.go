package interfaces

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func reader() {
	// r := strings.NewReader("Hello Gophers! Readers are awesome")
	r, err := os.Open("test.txt")
	buf, err := ioutil.ReadAll(r)
	if err != nil {
		fmt.Printf("Error in reader: %v\n", err)
		return
	}

	fmt.Printf(string(buf))
}

func ReaderWriterMain() {
	reader()
	resp, err := http.Get("https://golang.org")
	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()

	f, _ := os.Create("golang.html")
	defer f.Close()

	io.Copy(f, resp.Body)
}
