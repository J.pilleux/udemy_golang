package interfaces

import "fmt"

type Instrumenter interface {
	Play()
}

type Amplifier interface {
	Connect(amp string)
}

type Guitar struct {
	Strings int
}

func (g *Guitar) Play() {
	fmt.Printf("Guitar with %d strings\n", g.Strings)
}

func (g *Guitar) Connect(amp string) {
	fmt.Printf("Connected to %s\n", amp)
}

type Piano struct {
	Keys int
}

func (p *Piano) Play() {
	fmt.Printf("Piano with %d strings\n", p.Keys)
}

func MainInterface() {
	var inst Instrumenter
	inst = &Guitar{6}

	inst.Play()

	inst = &Piano{32}
	inst.Play()

	g := Guitar{6}
	g.Play()
	g.Connect("Marshall")
}
